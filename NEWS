Bonobo-Media 0.2: "Funky Monkey"
================================

A new release of Bonobo-Media is available at
http://cactus.rulez.org/projects/bonobo-media

What is Bonobo-Media?
---------------------
Bonobo-Media is a set of Bonobo interfaces and a GTK+/libbonobo-based
implementation for the the control of generic multimedia playback. It
currently supports audio and video streams. Support for tuners is also
planned.
      
What is Bonobo-Media not?
-------------------------
It is not a solution for actually delivering the media content to
the end-user. You should use other infrastructures like the GNOME
Multimedia Framework for that, ideally, from a Bonobo-Media stream
provider.

What's up with this release?
----------------------------
Features of this release include:

 * The CORBA interface definition of Bonobo::Media::Stream, ::Audio,
   and ::Video
 * A GtkObject/C-based generic implementation of these interfaces
   (just subclass the provided objects to create new stream providers)
 * A GTK+ widget for stream control, and one for media content control
   (including audio and video)
 * A Bonobo control wrapper around said widget, implementing
   PersistFile and PersistStream so you can load your media content
   directly to the control

Changes since the last release include:

 * The implementation is now based on BonoboXObject, so creating new
   implementations is made even more simple
 * For efficiency reasons, some CORBA methods are changed to be
   one-way

But wait, there's more! Download now, and get not just one, but
TWO stream providers in the package!

 * An SMPEG-based audio stream provider (for playback of MP3 files)
 * An Ogg Vorbis-based audio stream provider

Of course, in the tradition of `Special Offers Available Only If You
Download Now', only Vorbis works 100%: the SMPEG provider doesn't
use EsounD yet (this means the actual sound playback will occur on the
machine where the factory is run)

Wim Taymans of the GStreamer project is also working on a GStreamer
provider for Bonobo-Media. GStreamer is a very exciting media
framework project that is progressing very fast. Be sure to check it
out.
    
Who should use this release?
----------------------------
It's a stable release, the interfaces and the API are never going to
change, that's why it's called a 1.0 release!
Oh wait, it is _not_ called a 1.0. Guess that means you shouldn't
expect it to show any sign of maturity. In fact, I'm pretty sure some
aspects of the interfaces aren't perfectly `right' yet.
So, you should only use this release if you want to experiment with
it, or if you are ready to change your code at the time of the next
Bonobo-Media release.
