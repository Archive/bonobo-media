/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* $Id$
 *
 * media-player: A test program for media-player-control
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License  along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 *
 * Copyright (c) 2000-2001 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 */

#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>

#include "media-player-stream.h"
#include "media-player-video.h"

#include <config.h>

static gboolean main_window_destroy_cb (GtkObject *caller,
					gpointer   user_data)
{
    gtk_main_quit ();
    
    return FALSE;
}

static void setup_audio (Bonobo_Media_Stream media_stream)
{
	CORBA_Environment   ev;
	Bonobo_Media_Audio  audio;
	gchar              *esdspeaker;

	esdspeaker = getenv ("ESDSPEAKER") ? g_strdup (getenv ("ESDSPEAKER")) : 0;
	
#define HOSTLENGTH 255
	if (!esdspeaker)
	{
		gchar *esdhost = g_new (gchar, HOSTLENGTH + 1);
		gethostname (esdhost, HOSTLENGTH);
		esdspeaker = g_strdup_printf ("%s:16001", esdhost);
		g_free (esdhost);
	}
	
	CORBA_exception_init (&ev);
	audio = Bonobo_Unknown_queryInterface
		(media_stream, "IDL:Bonobo/Media/Audio:1.0", &ev);
	if (ev._major == CORBA_NO_EXCEPTION &&
	    audio != CORBA_OBJECT_NIL)
	{
		Bonobo_Media_Audio_setSpeaker (audio, esdspeaker, &ev);
		Bonobo_Unknown_unref (audio, &ev);
	}
	CORBA_exception_free (&ev);
	
	g_free (esdspeaker);
}

static GtkWidget * create_control (gchar *filename)
{
	CORBA_Environment    ev;
        Bonobo_Unknown       object;
        Bonobo_Media_Stream  media_stream;
	GtkWidget           *stream_control;
	GtkWidget           *video_control;
	GtkWidget           *control;

	if (!strchr (filename, ':'))
		filename = g_strdup_printf ("file:%s", filename);

	CORBA_exception_init (&ev);
        object = bonobo_get_object (filename, "IDL:Bonobo/Media/Stream:1.0", &ev);
        media_stream = Bonobo_Unknown_queryInterface (object, "IDL:Bonobo/Media/Stream:1.0", &ev);
	CORBA_exception_free (&ev);

        Bonobo_Unknown_unref (object, 0);

	control = gtk_vbox_new (FALSE, 5);
	
	/* Set up audio stuff */
	setup_audio (media_stream);

	/* Video display */
	video_control = media_player_video_new ();
	media_player_video_set_stream (MEDIA_PLAYER_VIDEO (video_control),
				       media_stream);
	gtk_container_add (GTK_CONTAINER (control), video_control);
	
	/* Stream controller (play, pause, stop, position) */
	stream_control = media_player_stream_new ();
	media_player_stream_set_stream (MEDIA_PLAYER_STREAM (stream_control),
					media_stream);
	gtk_container_add (GTK_CONTAINER (control), stream_control);
				       
        Bonobo_Unknown_unref (media_stream, 0);

	gtk_widget_show_all (control);
        return control;
}

static void create_ui (gchar *filename)
{
    GtkWidget *window, *control;

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "GNOME Media Player");
    gtk_signal_connect (GTK_OBJECT (window), "destroy",
			GTK_SIGNAL_FUNC (main_window_destroy_cb), NULL);
    
    control = create_control (filename);
    gtk_container_add (GTK_CONTAINER (window), control);

    gtk_widget_show_all (window);
}

/* We need to be in the main loop to set up the control */
static gboolean idle_cb (gpointer user_data)
{
    gchar *filename = (gchar*) user_data;
    
    create_ui (filename);   

    return FALSE;
}

int main (int argc, char **argv)
{
    poptContext   ctx;
    const char  **args;
    CORBA_ORB     orb;

    gnome_init_with_popt_table ("media-player", VERSION, argc, argv,
				NULL, 0, &ctx);
    args = poptGetArgs (ctx);
    if (args)
    {
	for (; *args; args++)
	    gtk_idle_add (idle_cb, (gpointer)*args);
    }
    
    orb = oaf_init (argc, argv);
    if (!bonobo_init (orb, NULL, NULL))
	g_error ("bonobo_init failed");
    
    bonobo_main ();

    return 0;    
}
