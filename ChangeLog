2001-07-09  ERDI Gergo  <cactus@cactus.rulez.org>

	* media-player/media-player-video.gob: Show video in the media player

2001-07-03  ERDI Gergo  <cactus@cactus.rulez.org>

	* bonobo-media/bonobo-media-util.c (bonobo_media_play_stream): New
	client-side helper function to play a stream in a "fire & forget" way

2001-05-06  ERDI Gergo  <cactus@cactus.rulez.org>

	* configure.in:
	* Makefile.am: create and install bonobo-mediaConf.sh

2001-04-25  ERDI Gergo  <cactus@cactus.rulez.org>

	* idl/Bonobo-Media.idl: Changed stream controller methods to be
	one-way

2001-04-15  ERDI Gergo  <cactus@cactus.rulez.org>

	* bonobo-media/bonobo-media-video.gob: Ported to BonoboXObject

	* bonobo-media/bonobo-media-audio.gob: Removed pre-BonoboXObject cruft

2001-04-13  ERDI Gergo  <cactus@cactus.rulez.org>

	* configure.in: Bumped version number and so version
	(GOB_CHECK): check for a recent GOB (that supports overriding
	get_type)
	(AM_PATH_GSTREAMER): Check for GStreamer
	(AC_OUTPUT): Create stream-providers/gstream/Makefile

	* bonobo-media/bonobo-media-audio.gob: migrating to BonoboXObject

	* bonobo-media/bonobo-media-stream.gob (send_play_notification):
	(send_stop_notification): Utility functions for notifying clients
	of state changes

	* stream-providers/vorbis/bonobo-media-vorbis.gob: Notify clients
	via send_{play,stop}_notification when starting/stopping playback


	* idl/Bonobo-Media.idl: Changed play, stop, and seek to be oneway
	methods (the actual state change is told to clients by an
	EventSource)

2001-01-22  ERDI Gergo  <cactus@cactus.rulez.org>

	* configure.in:	
	* stream-providers/smpeg/Makefile.am:
	* stream-providers/vorbis/Makefile.am: Compile stream providers
	only when appropriate libraries are found

2001-01-21  ERDI Gergo  <cactus@cactus.rulez.org>

	* bonobo-media/bonobo-media-audio.gob: Fire a signal when the ESD
	speaker changes

	* stream-providers/vorbis/bonobo-media-vorbis.gob: Use
	conditionals in player loop to reduce CPU usage

	* bonobo-media/bonobo-media-control.gob: Use default ESD port of 16001

	* stream-providers/vorbis/bonobo-media-vorbis.gob: Do audio output
	to the EsounD speaker specified via the Audio interface

2001-01-17  ERDI Gergo  <cactus@cactus.rulez.org>

	* stream-providers/vorbis/bonobo-media-vorbis.gob: Do actual
	playback with EsounD (no B::M::Audio support yet, just stream to
	the local ESD server)

	* media-player/media-player-control.gob (PersistFile_load): Do not
	prepend "file:" by default (to support loading via e.g. http)

2001-01-16  ERDI Gergo  <cactus@cactus.rulez.org>

	* stream-providers/smpeg/bonobo-media-smpeg.gob: Couple of
	assertions to check for a valid SMPEG object before
	seeking/playing/etc.

	* bonobo-media/bonobo-media-control.gob: Moved video setup
	functions to a separate method; New method to set up the sound
	channel

	* NEWS: Getting ready for a 1.0 release

2001-01-15  ERDI Gergo  <cactus@cactus.rulez.org>

	* idl/Bonobo-Media.idl: Remove StreamListener, you should use
	generic Bonobo::EventSource for this

	* bonobo-media/bonobo-media-stream.gob: 
	* bonobo-media/bonobo-media-control-stream.gob: Use EventSources
	and Listeners instead of StreamListeners

	* stream-providers/smpeg/bonobo-media-smpeg.oafinfo:
	* stream-providers/vorbis/bonobo-media-vorbis.oafinfo: Add entry
	about Bonobo/EventSource interface
	
2001-01-14  ERDI Gergo  <cactus@cactus.rulez.org>

	* bonobo-media/bonobo-media-control-stream.gob: Bunch of checks to
	see if we have a valid stream to control

	* idl/Bonobo-Media.idl: Added new method to Audio to set the
	remote ESD speaker

2001-01-13  ERDI Gergo  <cactus@cactus.rulez.org>

	* README: Added some general information, plus a pointer to the
	bonobo-media web page

2001-01-11  ERDI Gergo  <cactus@cactus.rulez.org>

	* media-player/media-player-control-factory.c: 
	* media-player/media-player-control.oafinfo: Factory for generic
	Bonobo Media stream playback control

	* stream-providers/smpeg/bonobo-media-smpeg-factory.c
	(smpeg_stream_factory): Removed all occurances of `mpg123' (legacy
	from the pervious mp3 stream provider)

2001-01-09  ERDI Gergo  <cactus@cactus.rulez.org>

	* bonobo-media/bonobo-media-control-stream.gob: The scale is of
	the correct length now (FIXME: the SMPEG provider seems to crash
	when seeking to the end of the stream)

2001-01-06  Ali Abdin  <aliabdin@aucegypt.edu>

	* stream-providers/vorbis/bonobo-media-vorbis.gob:
	* stream-providers/vorbis/Makefile.am:
	* stream-providers/vorbis/bonobo-media-vorbis.oafinfo:
	* stream-providers/vorbis/bonobo-media-vorbis-factory.c:
	* stream-providers/Makefile.am:
	* stream-providers/configure.in:

	Initial check-in for Ogg Vorbis support. Still some stuff to do, but I
	will be travelling soon, so I want to get my work out there (and I
	have permission to check-in)

2001-01-03  ERDI Gergo  <cactus@cactus.rulez.org>

	* media-player/media-player-control.gob: Implement PersistStream

2000-12-30  ERDI Gergo <cactus@cactus.rulez.org>

	* bonobo-media/bonobo-media-control.gob:
	* bonobo-media/bonoob-media-control-stream.gob:
	* bonobo-media/bonobo-media-stream-listener.gob: Allow changing
	the associated stream after creation time

	* media-player/media-player-control.gob: use these stream changing
	functions in the PersistFile::load implementation
		
2000-12-28  ERDI Gergo  <cactus@cactus.rulez.org>

	* bonobo-media/bonobo-media-stream.gob: New utility functions to
	send position/stream end notification to all listeners

2000-12-26  ERDI Gergo  <cactus@cactus.rulez.org>

	* idl/Bonobo-Media.idl: StreamListener::notifyEnd method to tell
	players the stream ended

2000-12-24  ERDI Gergo  <cactus@cactus.rulez.org>

	* configure.in bonobo-media/Makefile.am
	stream-providers/mpg123/Makefile.am: check for GOB

	* bonobo-media/bonobo-media-stream-listener.gob: Proper management
	of the lifecycle of the stream

	* bonobo-media/bonobo-media-control.gob: stop the stream and unref
	in the destructor

	* bonobo-media/bonobo-media-control-{stream,volume}.gob: unref the
	underlying Bonobo component in the destructor

	* bonobo-media/bonobo-media-stream-listener.gob: Changed direct
	access to BONOBO_OBJECT->corba_objref to use accessor method

