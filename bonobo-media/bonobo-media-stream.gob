/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

%at{
/* $Id: bonobo-media-stream.gob,v 1.10 2001/04/25 20:06:03 cactus Exp $
 *
 * bonobo-media-stream: basic abstract class for Bonobo::Media::Stream
 *                      implementations
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License  along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 *
 * Copyright (c) 2000-2001 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 */
%}

%header{
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-event-source.h>
#include <bonobo-media/Bonobo-Media.h>
%}

%{
#include <bonobo/bonobo-arg.h>
	
POA_Bonobo_Media_Stream__vepv bonobo_media_stream_vepv;
%}

class Bonobo:Media:Stream from Bonobo:Object {

	private Bonobo:EventSource *event_source;

	protected void send_play_notification (self)
		{
			BonoboArg *arg;

			arg = bonobo_arg_new (BONOBO_ARG_NULL);

			bonobo_event_source_notify_listeners_full
				(self->_priv->event_source,
				 "Bonobo/Media/Stream",
				 "play_notify", 0,
				 arg, 0);

			bonobo_arg_release (arg);
		}

	protected void send_stop_notification (self)
		{
			BonoboArg *arg;

			arg = bonobo_arg_new (BONOBO_ARG_NULL);

			bonobo_event_source_notify_listeners_full
				(self->_priv->event_source,
				 "Bonobo/Media/Stream",
				 "stop_notify", 0,
				 arg, 0);

			bonobo_arg_release (arg);
		}
	
	protected void send_end_notification (self)
		{
			BonoboArg *arg;

			arg = bonobo_arg_new (BONOBO_ARG_NULL);
			
			bonobo_event_source_notify_listeners_full
				(self->_priv->event_source,
				 "Bonobo/Media/Stream",
				 "end_notify", 0,
				 arg, 0);

			bonobo_arg_release (arg);
		}

	
	protected void send_pos_notification (self, int pos)
		{
			BonoboArg *arg;

			arg = bonobo_arg_new (BONOBO_ARG_LONG);
			BONOBO_ARG_SET_LONG (arg, pos);
			
			bonobo_event_source_notify_listeners_full
				(self->_priv->event_source,
				 "Bonobo/Media/Stream",
				 "position_notify", 0,
				 arg, 0);

			bonobo_arg_release (arg);
		}
	
	virtual gint get_length (self, CORBA_Environment *ev)
		{
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Media_Stream_NotSupported, NULL);
			return 0;
		}
	
	virtual gfloat get_pos_per_sec (self, CORBA_Environment *ev)
		defreturn 1;

	virtual public void seek (self, gint pos, CORBA_Environment *ev)
		{
			CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
					     ex_Bonobo_Media_Stream_NotSupported, NULL);
		}
	
	virtual public void play (self, CORBA_Environment *ev);
	virtual public void stop (self, CORBA_Environment *ev);
	
	private CORBA_float impl_Bonobo_Media_Stream_getPositionsPerSecond (PortableServer_Servant  servant,
									    CORBA_Environment      *ev)
		{
			BonoboMediaStream *self;
			self = BONOBO_MEDIA_STREAM (bonobo_object_from_servant (servant));
			
			return get_pos_per_sec (self, ev);
		}

	private CORBA_long impl_Bonobo_Media_Stream_getLength (PortableServer_Servant  servant,
							       CORBA_Environment      *ev)
		{
			BonoboMediaStream *self;
			self = BONOBO_MEDIA_STREAM (bonobo_object_from_servant (servant));
			
			return get_length (self, ev);
		}
	
	private void impl_Bonobo_Media_Stream_seek (PortableServer_Servant  servant,
						    const CORBA_long        pos,
						    CORBA_Environment      *ev)
		{
			BonoboMediaStream *self;
			self = BONOBO_MEDIA_STREAM (bonobo_object_from_servant (servant));
			
			seek (self, pos, ev);
		}
	
	private void impl_Bonobo_Media_Stream_stop (PortableServer_Servant  servant,
						    CORBA_Environment      *ev)
		{
			BonoboMediaStream *self;
			self = BONOBO_MEDIA_STREAM (bonobo_object_from_servant
							    (servant));
			
			stop (self, ev);
			
		}

	private void impl_Bonobo_Media_Stream_play (PortableServer_Servant  servant,
						    CORBA_Environment      *ev)
		{
			BonoboMediaStream *self;
			self = BONOBO_MEDIA_STREAM (bonobo_object_from_servant (servant));

			play (self, ev);
		}

	public POA_Bonobo_Media_Stream__epv * get_epv (void)
		{
			POA_Bonobo_Media_Stream__epv *epv;
			
			epv = g_new0 (POA_Bonobo_Media_Stream__epv, 1);
			
			epv->getLength             = impl_Bonobo_Media_Stream_getLength;
			epv->getPositionsPerSecond = impl_Bonobo_Media_Stream_getPositionsPerSecond;
			epv->seek                  = impl_Bonobo_Media_Stream_seek;
			epv->stop                  = impl_Bonobo_Media_Stream_stop;
			epv->play                  = impl_Bonobo_Media_Stream_play;
			
			return epv;
		}
	
	private void init_media_stream_corba_class (void)
		{
			/* The VEPV */
			bonobo_media_stream_vepv.Bonobo_Unknown_epv  = bonobo_object_get_epv ();
			bonobo_media_stream_vepv.Bonobo_Media_Stream_epv = bonobo_media_stream_get_epv ();
		}


	class_init (class)
		{
			init_media_stream_corba_class ();
		}
	
	public Bonobo_Media_Stream corba_object_create (Bonobo:Object *object)
		{
			POA_Bonobo_Media_Stream *servant;
			CORBA_Environment ev;
			
			servant = (POA_Bonobo_Media_Stream *) g_new0 (BonoboObjectServant, 1);
			servant->vepv = &bonobo_media_stream_vepv;
			
			CORBA_exception_init (&ev);
			
			POA_Bonobo_Media_Stream__init ((PortableServer_Servant) servant, &ev);
			if (ev._major != CORBA_NO_EXCEPTION)
			{
				g_free (servant);
				CORBA_exception_free (&ev);
				return CORBA_OBJECT_NIL;
			}
			
			CORBA_exception_free (&ev);
			
			return bonobo_object_activate_servant (object, servant);
		}
	
	public BonoboMediaStream* construct (self,
					     Bonobo_Media_Stream corba_stream (check null))
		{
			BonoboEventSource *event_source;
			
			bonobo_object_construct (BONOBO_OBJECT (self),
						 corba_stream);

			event_source = bonobo_event_source_new ();
			bonobo_object_add_interface (BONOBO_OBJECT (self),
						     BONOBO_OBJECT (event_source));

			self->_priv->event_source = event_source;
			
			return self;
		}
}
